import gym

if __name__ == '__main__':
    env = gym.make('BipedalWalker-v3')
    observation = env.reset()
    for t in range(1000):
        env.render()
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        print(observation)
        print(f"Reward {reward}")
        if done:
            print("Finish")
            break
    print(env.action_space)
    env.close()
